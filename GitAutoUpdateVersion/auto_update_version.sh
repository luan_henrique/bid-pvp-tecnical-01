#!/bin/bash
read versionFilePath < "GitAutoUpdateVersion/versionFilePath.txt"
read versionValue < $versionFilePath
numbers=($versionValue)
numlen=${#numbers[*]}

if [ $numlen -eq 3 ]; then
    if [ $1 = patch ]; then
        numbers[2]=$((${numbers[2]} + 1))
    elif [ $1 = minor ]; then
        numbers[1]=$((${numbers[1]} + 1))
        numbers[2]=$((0))
    elif [ $1 = major ]; then
        numbers[0]=$((${numbers[0]} + 1))
        numbers[1]=$((0))
        numbers[2]=$((0))
    else
        echo "argument must be version increment() receved was: \"$1\"" 
    fi
else
    numbers=($"0 0 0")
fi
echo "version: " ${numbers[*]}
echo ${numbers[*]} > $versionFilePath