﻿using UnityEngine;
using UnityEngine.UI;

namespace BidPVP.Scripts.GeneralProps
{
    public class AuctioneerBehaviour : MonoBehaviour
    {
        private static readonly int Message = Animator.StringToHash("Message");
        public Animator auctioneerAnimator;
        public Text auctioneerText;
        public Image bg;

        private void Start()
        {
            Invoke(nameof(ClearText), 3);
        }

        private void ClearText()
        {
            auctioneerText.text = "";
            bg.enabled = false;
        }

        public void ShowText(string text, double time = 1)
        {
            auctioneerAnimator.SetTrigger(Message);
            auctioneerText.text = text;
            bg.enabled = true;
            if (IsInvoking(nameof(ClearText))) CancelInvoke(nameof(ClearText));
            Invoke(nameof(ClearText), (float) time);
        }
    }
}