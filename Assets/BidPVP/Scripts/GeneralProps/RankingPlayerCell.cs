﻿using UnityEngine;
using UnityEngine.UI;

namespace BidPVP.Scripts.GeneralProps
{
    public class RankingPlayerCell : MonoBehaviour
    {
        public Animator PlayerCellAnimator;

        public Text playerName;

        public Text positionNumber;

        public Text totalEarning;


        public void SetPlayerName(string name)
        {
            playerName.text = name;
        }

        public void SetTotalEarning(string earning)
        {
            totalEarning.text = earning;
        }

        public void SetPositionNumber(string position)
        {
            positionNumber.text = position;
        }

        public void EnableWinnerElements()
        {
            PlayerCellAnimator.SetTrigger("Win");
        }
    }
}