﻿using Photon.Pun;

namespace BidPVP.Scripts.GeneralProps
{
    public class ExitButton : MonoBehaviourPun
    {
        public void Disconnect()
        {
            PhotonNetwork.LeaveRoom();
        }
    }
}