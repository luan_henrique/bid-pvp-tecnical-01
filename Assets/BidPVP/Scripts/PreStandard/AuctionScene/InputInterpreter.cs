﻿using BidPVP.Scripts.PreStandard.LogicAuction;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace BidPVP.Scripts.PreStandard.AuctionScene
{
    public class InputInterpreter : MonoBehaviourPun
    {
        public Animator door;
        public void BidButton()
        {
            if (!door.GetCurrentAnimatorStateInfo(0).IsName("storage_door_open")) return;
            PhotonNetwork.RaiseEvent(
                (byte) AuctionEventType.PlayerPressBid, 
                PhotonNetwork.NickName, 
                new RaiseEventOptions() {Receivers = ReceiverGroup.MasterClient},
                SendOptions.SendReliable
            );
        }
    }
}