﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace BidPVP.Scripts.PreStandard.AuctionScene
{
    public class StorageSoldAnimation : MonoBehaviourPun
    {
        public Text storageValueLabel;
        public Text playerName;
        public Text profitLabel;
        public Text payedValueLabel;

        public void ShowFeedback(int storageValue, int payedValue, string playerName)
        {
            this.playerName.text = "Sold to " + playerName;
            storageValueLabel.text = "$" + storageValue;
            payedValueLabel.text = "$" + payedValue;
            profitLabel.text = "profit: $" + (storageValue - payedValue);
            GetComponent<Animator>().SetTrigger(payedValue > storageValue ? "auctionLoss" : "auctionWin");
        }

        public void DisplayEnd()
        {
            bool finished;
            if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue("auctionFinished", out var isFinished))
                finished = (bool) isFinished;
            else
                finished = false;

            if (!PhotonNetwork.IsMasterClient) return;
        
            if (!finished) 
                PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable() {{"openStorage", true}});
            else
                PhotonNetwork.LoadLevel("Ranking");
        }
    }
}