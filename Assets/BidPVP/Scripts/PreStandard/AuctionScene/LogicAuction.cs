﻿using System.Linq;
using BidPVP.Scripts.DataModules;
using BidPVP.Scripts.PreStandard.LogicAuction;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Player = Photon.Realtime.Player;

namespace BidPVP.Scripts.PreStandard.AuctionScene
{
    public class LogicAuction : MonoBehaviourPunCallbacks, IOnEventCallback
    {
        public Auction auction;
        public storageBackItems backList;
        public storageFrontItems frontList;
        public storageMiddleItems middleList;

        public void OnEvent(EventData photonEvent)
        {
            if (!PhotonNetwork.IsMasterClient) return;

            if (photonEvent.Code == (byte) AuctionEventType.PlayerPressBid)
                auction.ReceiveBid((string) photonEvent.CustomData);
        }


        private void Awake()
        {
            PhotonPeer.RegisterType(typeof(Auction), 255, Auction.Serialize, Auction.Deserialize);
        }

        private void Start()
        {
            if (!PhotonNetwork.IsConnected)
            {
                PhotonNetwork.LoadLevel("Lobby_Menu");
                return;
            }

            if (!PhotonNetwork.IsMasterClient) return;

            var auctioneerTime = PlayerPrefs.GetFloat(Launcher.AuctioneerTimeSelector.AuctioneerTimePlayerPrefKey);
            var numPlayers = PlayerPrefs.GetInt("numPlayersHostConfig");
            var numStorage = PlayerPrefs.GetInt("numStorageHostConfig");
            
            PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable()
            {
                {"currentStorageNumber", 0},
                {"totalStorageNumber", numStorage}
            });
            
            auction = new Auction(auctioneerTime, numStorage, numPlayers);

            foreach (var storageUnit in auction.StorageUnits)
            {
                var frontItem =
                    frontList.StorageFrontItems[Random.Range(0, frontList.StorageFrontItems.Count - 1)];
                var front = storageUnit.Items[0];
                front.Value = frontItem.itemPrice;
                front.name = frontItem.name;

                var middleItem =
                    middleList.StorageMiddleItems[Random.Range(0, middleList.StorageMiddleItems.Count - 1)];
                var middle = storageUnit.Items[1];
                middle.Value = middleItem.itemPrice;
                middle.name = middleItem.name;

                var backItem =
                    backList.StorageBackItems[Random.Range(0, backList.StorageBackItems.Count - 1)];
                var back = storageUnit.Items[2];
                back.Value = backItem.itemPrice;
                back.name = backItem.name;
            }

            var me = PhotonNetwork.LocalPlayer;

            var masterStartMoney = PlayerPrefs.GetInt("startingMoney");
            var newProps = new Hashtable {{"money", masterStartMoney}};
            me.SetCustomProperties(newProps);
            auction.PlayerJoin(PhotonNetwork.NickName, masterStartMoney);
        }

        private void Update()
        {
            PhotonNetwork.NetworkingClient.Service();

            if (!PhotonNetwork.IsMasterClient) return;

            if (auction.Events != null)
            {
                foreach (var auctionEvent in auction.Events.ConsumeStream()) ProcessEvent(auctionEvent, auction);
            }

            if (PhotonNetwork.IsMasterClient) auction.PassTime(Time.deltaTime);

            foreach (var auctionEvent in auction.Events.ConsumeStream()) ProcessEvent(auctionEvent, auction);
        }


        private static void ProcessEvent(AuctionEvent auctionEvent, Auction auction)
        {
            var eventType = auctionEvent.Type;
            switch (eventType)
            {
                case AuctionEventType.PlayerJoin:
                    ProcessPlayerJoinEvent(auctionEvent, eventType);
                    break;
                case AuctionEventType.GameStart:
                    ProcessAuctionStartEvent(auction, eventType);
                    break;
                case AuctionEventType.Bid:
                    ProcessBidEvent(auctionEvent, eventType);
                    break;
                case AuctionEventType.StorageSold:
                    ProcessStorageSoldEvent(auctionEvent, auction, eventType);
                    break;
                case AuctionEventType.GameEnd:
                    SendToAll(eventType, new object[] { });
                    break;
                case AuctionEventType.PlayerLeave:
                    SendToAll(eventType, new[] {auctionEvent.Data["name"]});
                    break;
                case AuctionEventType.NotifyRemainingTime:
                    SendToAll(eventType, auctionEvent.Data["remainingRime"]);
                    break;
                case AuctionEventType.PlayerPressBid:
                    break;
            }
        }

        private static void ProcessStorageSoldEvent(AuctionEvent auctionEvent, Auction auction,
            AuctionEventType eventType)
        {
            if (!auction.finished)
                PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable
                {
                    {"items", auction.StorageUnits[auction.currentStorage].Items.Select(item => item.name).ToArray()},
                    {"openStorage", false},
                    {"currentStorageNumber", auction.currentStorage + 1},
                    {"auction", Auction.Serialize(auction)}
                });
            else
                PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable
                    {
                        {"openStorage", false},
                        {"auctionFinished", true},
                        {"auction", Auction.Serialize(auction)}
                    }
                );

            SendToAll(eventType, new[]
            {
                auctionEvent.Data["storageValue"],
                auctionEvent.Data["price"],
                auctionEvent.Data["player"]
            });

            Player buyerNetworkPlayer = null;

            foreach (var playerField in PhotonNetwork.CurrentRoom.Players)
                if (ReferenceEquals(playerField.Value.NickName, auctionEvent.Data["player"]))
                {
                    buyerNetworkPlayer = playerField.Value;
                    break;
                }

            var auctionPlayer = auction.GetPlayerOfName((string) auctionEvent.Data["player"]);
            var money = auctionPlayer.Money;
            var earning = auctionPlayer.Earnings;
            var spentMoney = auctionPlayer.SpentMoney;


            if (buyerNetworkPlayer != null)
                buyerNetworkPlayer.SetCustomProperties(
                    new Hashtable
                    {
                        {"money", money},
                        {"earnings", earning},
                        {"spentMoney", spentMoney}
                    }
                );
        }


        private static void ProcessBidEvent(AuctionEvent auctionEvent, AuctionEventType eventType)
        {
            SendToAll(eventType, new[]
            {
                auctionEvent.Data["player"],
                auctionEvent.Data["value"]
            });
        }

        private static void ProcessAuctionStartEvent(Auction auction, AuctionEventType eventType)
        {
            var itemContents = new Hashtable
            {
                {
                    "items",
                    auction.StorageUnits[auction.currentStorage].Items.Select(item => item.name).ToArray()
                },
                {"openStorage", true},
                {"currentStorageNumber", auction.currentStorage + 1},
                {"auction", Auction.Serialize(auction)}
            };
            PhotonNetwork.CurrentRoom.SetCustomProperties(itemContents);
            PhotonNetwork.CurrentRoom.IsOpen = false;
            SendToAll(eventType, new object[] { });
        }

        private static void ProcessPlayerJoinEvent(AuctionEvent auctionEvent, AuctionEventType eventType)
        {
            SendToAll(eventType, new[] {auctionEvent.Data["name"]});
        }

        private static void SendToAll(AuctionEventType evCode, object content)
        {
            var raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
            PhotonNetwork.RaiseEvent((byte) evCode, content, raiseEventOptions, SendOptions.SendReliable);
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            if (!PhotonNetwork.IsMasterClient) return;

            if (newPlayer.CustomProperties.TryGetValue("startingMoney", out var playerStartingMoney))
            {
                newPlayer.SetCustomProperties(new Hashtable
                {
                    {"money", playerStartingMoney},
                    {"earnings", 0}
                });
                auction.PlayerJoin(newPlayer.NickName, (int) playerStartingMoney);
            }
        }

        public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
        {
            if (PhotonNetwork.NickName == target.NickName)
            {
                int? money = null;
                int? earnings = null;
                var shouldUpdate = false;
                foreach (var changedProp in changedProps)
                {
                    if ((string) changedProp.Key == "money")
                    {
                        shouldUpdate = true;
                        money = (int) changedProp.Value;
                        continue;
                    }

                    if ((string) changedProp.Key == "earnings")
                    {
                        shouldUpdate = true;
                        earnings = (int) changedProp.Value;
                    }
                }

                if (shouldUpdate)
                {
                    if (money == null) money = (int?) target.CustomProperties["money"];
                    if (earnings == null) earnings = (int?) target.CustomProperties["earnings"];

                    if (money != null && earnings != null)
                    {
                        Debug.Log($"startingMoney({(int) money + (int) earnings}, money({money}, earnings({earnings})");
                        var startingMoney = (int) money + (int) earnings;
                        PlayerPrefs.SetInt("startingMoney", startingMoney);
                    }
                }
            }


            if (!PhotonNetwork.IsMasterClient) return;

            foreach (var changedProp in changedProps)
            {
                if ((string) changedProp.Key != "startingMoney") continue;

                var LogicPlayer = auction.GetPlayerOfName(target.NickName);

                if (LogicPlayer == null)
                {
                    target.SetCustomProperties(new Hashtable
                    {
                        {"money", changedProp.Value},
                        {"earnings", 0}
                    });
                    auction.PlayerJoin(target.NickName, (int) changedProp.Value);
                }
            }
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
        }


        public override void OnMasterClientSwitched(Player newMasterClient)
        {
            if (!Equals(newMasterClient, PhotonNetwork.LocalPlayer)) return;
            var serializedAuction = (byte[]) PhotonNetwork.CurrentRoom.CustomProperties["auction"];
            auction = Auction.Deserialize(serializedAuction);
        }


        public override void OnLeftRoom()
        {
            PhotonNetwork.LoadLevel("Lobby_Menu");
        }
    }
}