﻿using System.Collections.Generic;
using BidPVP.Scripts.DataModules;
using BidPVP.Scripts.GeneralProps;
using BidPVP.Scripts.Launcher;
using BidPVP.Scripts.PreStandard.LogicAuction;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using Player = Photon.Realtime.Player;

namespace BidPVP.Scripts.PreStandard.AuctionScene
{
    public class AuctionEventInterpreter : MonoBehaviourPunCallbacks, IOnEventCallback
    {
        private static readonly int OpenStorage = Animator.StringToHash("openStorage");
        private bool _waitingItemUpdates;

        public AuctioneerBehaviour auctioneer;

        private List<GameObject> badges;
        
        public GameObject charPrefab;
        
        
        private GameObject container_char;
        private GameObject ItemBackLayer;
        private GameObject ItemFrontLayer;
        private GameObject ItemMiddleLayer;
        
        public storageMiddleItems MiddleItems;
        public storageFrontItems FrontItems;
        public storageBackItems BackItems;
        
        public GameObject MoneyHolder;

        public GameObject PopupSold;

        public Animator storageDoorAnimator;

        private int currentStorageNumber;
        private int totalStorageNumber;
        private Text _storageSequenceIndicator;

        public void OnEvent(EventData photonEvent)
        {
            if (photonEvent.Code >= 20) return;

            var customData = photonEvent.CustomData;
            switch ((AuctionEventType) photonEvent.Code)
            {
                case AuctionEventType.PlayerJoin:
                    CreatePlayerBadge(((object[]) customData)[0].ToString());
                    break;
                case AuctionEventType.GameStart:
                    break;
                case AuctionEventType.Bid:
                    ClearBidBalloons();
                    ShowBidBalloon(((object[]) customData)[0], ((object[]) customData)[1]);
                    auctioneer.ShowText($"${((object[]) customData)[1]} para {((object[]) customData)[0]}", 2);
                    break;
                case AuctionEventType.StorageSold:
                    PopupSold.GetComponent<StorageSoldAnimation>().ShowFeedback((int) ((object[]) customData)[0],
                        (int) ((object[]) customData)[1], (string) ((object[]) customData)[2]);
                    auctioneer.ShowText(
                        $" vendido por ${((object[]) customData)[1]} para {((object[]) customData)[2]}!!!", 1.5);
                    ClearBidBalloons();
                    break;
                case AuctionEventType.GameEnd:
                    break;
                case AuctionEventType.PlayerLeave:
                    DestroyBadge(((object[]) customData)[0].ToString());
                    break;
                case AuctionEventType.NotifyRemainingTime:
                    DisplayRemainingTime((double) customData);

                    break;
                case AuctionEventType.PlayerPressBid:
                    break;
                default:
                    Debug.Log("unknown event: " + photonEvent.Code);
                    break;
            }
        }

        private void getReferences()
        {
            _storageSequenceIndicator = GameObject.Find("StorageCountText").GetComponent<Text>();
            auctioneer = FindObjectOfType<AuctioneerBehaviour>();

            container_char = GameObject.Find("CharacterContainer");
            this.
            ItemFrontLayer = GameObject.Find("ItemsLayerFront");
            ItemBackLayer = GameObject.Find("ItemsLayerBack");
            ItemMiddleLayer = GameObject.Find("ItemsLayerMiddle");
        }
        
        private void Start()
        {
            getReferences();
            
            badges = new List<GameObject>();
            if (!PhotonNetwork.IsConnected || PhotonNetwork.CurrentRoom == null) return;
            foreach (var player in PhotonNetwork.CurrentRoom.Players) CreatePlayerBadge(player.Value.NickName);

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue("money", out var money))
                UpdatePlayerMoney(money);
            
            updateStorageNumber();
            
            if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue("items", out var items)) PopulateStorage(items);

            if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue("openStorage", out var open))
                storageDoorAnimator.SetBool(OpenStorage, (bool) open);
        }

        private GameObject GetBadge(string playerName)
        {
            foreach (var badge in badges)
                if (badge.GetComponentInChildren<Text>().text == playerName)
                    return badge;

            return null;
        }

        private bool BadgeExists(string playerName)
        {
            return GetBadge(playerName) != null;
        }

        private void DestroyBadge(string playerName)
        {
            var badge = GetBadge(playerName);

            if (badge == null) return;

            Destroy(badge);
        }

        private void CreatePlayerBadge(string playerName)
        {
            if (BadgeExists(playerName)) return;

            var character = Instantiate(charPrefab, container_char.transform);
            badges.Add(character);
            character.GetComponent<Badge>().SetName(playerName);
        }

        private void PopulateStorage(object items)
        {
            var castItems = (string[]) items;
            ChangeContent(castItems[0], ItemFrontLayer, FrontItems.StorageFrontItems);
            ChangeContent(castItems[1], ItemMiddleLayer, MiddleItems.StorageMiddleItems);
            ChangeContent(castItems[2], ItemBackLayer, BackItems.StorageBackItems);
        }

        private static void ChangeContent(string back, GameObject layer, IEnumerable<storageItem> items)
        {
            foreach (var item in items)
            {
                if (item.name != back) continue;
                foreach (Transform child in layer.transform) Destroy(child.gameObject);

                Instantiate(item.itemGameObject, layer.transform);
                break;
            }
        }

        private void DisplayRemainingTime(double remainingTime)
        {
            switch (remainingTime)
            {
                case 3:
                    auctioneer.ShowText("Dou-lhe uma!");
                    break;
                case 2:
                    auctioneer.ShowText("Dou-lhe duas!!!", 2.0);
                    break;
            }
        }

        private void ShowBidBalloon(object playerName, object value)
        {
            GetBadge((string) playerName).GetComponent<Badge>().SetBid((int) value);
        }

        private void ClearBidBalloons()
        {
            foreach (var badge in badges) badge.GetComponent<Badge>().CleanBalloon();
        }

        public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
        {
            if (!Equals(target, PhotonNetwork.LocalPlayer)) return;

            foreach (var changedProp in changedProps)
                if ((string) changedProp.Key == "money")
                    UpdatePlayerMoney(changedProp.Value);
        }

        private void updateStorageNumber()
        {
            
            if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue("totalStorageNumber",
                out var _totalStorageNumber))
                totalStorageNumber = (int) _totalStorageNumber;

            if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue("currentStorageNumber",
                out var _currentStorageNumber))
                currentStorageNumber = (int) _currentStorageNumber;
            
            _storageSequenceIndicator.text = $"{currentStorageNumber} / {totalStorageNumber}";
        }
        
        public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
        {
            foreach (var changedProp in propertiesThatChanged)
                switch (changedProp.Key)
                {
                    case "items":
                        if (!storageDoorAnimator.GetBool(OpenStorage))
                            PopulateStorage(changedProp.Value);
                        else
                            _waitingItemUpdates = true;

                        break;
                    case "openStorage":
                        if ((bool) changedProp.Value && _waitingItemUpdates &&
                            PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue("items", out var setItems))
                            PopulateStorage(setItems);

                        storageDoorAnimator.SetBool("openStorage", (bool) changedProp.Value);
                        break;
                    case "totalStorageNumber":
                        updateStorageNumber();
                        break;
                    case "currentStorageNumber":
                        updateStorageNumber();
                        break;
                }
        }

        private void UpdatePlayerMoney(object money)
        {
            MoneyHolder.GetComponentInChildren<Text>().text = money.ToString();

            PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue("earnings", out var earnings);
        }
    }
}