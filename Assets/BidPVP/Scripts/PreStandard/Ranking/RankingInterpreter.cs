﻿using System.Collections.Generic;
using System.Linq;
using BidPVP.Scripts.GeneralProps;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BidPVP.Scripts.PreStandard.Ranking
{
    public class RankingInterpreter : MonoBehaviourPunCallbacks
    {

        public GameObject resultList;

        public GameObject charPrefabResult;
    
        public Dictionary<string, double> rankingDictionary = new Dictionary<string, double>();

        private bool winnerElements = false;
        
    
        private void Awake()
        {
            var players = PhotonNetwork.CurrentRoom.Players;
            foreach (var player in players)
            {
                int earnings = 0;
                int spentMoney = 0;
                var nick = player.Value.NickName;
                if (player.Value.CustomProperties.TryGetValue("earnings", out var outEarnings))
                {
                    earnings = (int)outEarnings;
                }
                if (player.Value.CustomProperties.TryGetValue("spentMoney", out var outSpentMoney))
                {
                    spentMoney = (int)outSpentMoney;
                }
            
                rankingDictionary.Add(nick, earnings - spentMoney);  
            
            }
        
            var sortedDict = from entry in rankingDictionary orderby entry.Value descending select entry;
            var position = 1;
            foreach (var nameEarningsPair in sortedDict)
            {
                var cell = Instantiate(charPrefabResult, resultList.transform).GetComponent<RankingPlayerCell>();
                cell.SetPlayerName(nameEarningsPair.Key);
                if (nameEarningsPair.Value > 0)
                {
                    cell.SetTotalEarning($"+ $ {nameEarningsPair.Value}");
                }else if (nameEarningsPair.Value < 0)
                {
                    cell.SetTotalEarning($"- $ {-nameEarningsPair.Value}");
                }
                else
                {
                    cell.SetTotalEarning("$ 0");
                }
                cell.SetPositionNumber($"#{position}");

                if (winnerElements != true)
                {
                    cell.EnableWinnerElements();
                    winnerElements = true;
                }
            
                position++;
            }
        }

        private void Disconnect()
        {
            if (IsInvoking(nameof(Disconnect)))
            {
                CancelInvoke(nameof(Disconnect));
            }
            if(PhotonNetwork.CurrentRoom != null)
                PhotonNetwork.LeaveRoom();
        }
    
        public void ExitScene()
        {
            Disconnect();
            SceneManager.LoadScene("Lobby_Menu");
        }
    }
}
