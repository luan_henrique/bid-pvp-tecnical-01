using System;
using System.Collections.Generic;

namespace BidPVP.Scripts.PreStandard.LogicAuction
{
    [Serializable]
    public class AuctionEvent
    {
        public AuctionEventType Type;
        public Dictionary<string, object> Data;

        public AuctionEvent(AuctionEventType type)
        {
            Type = type;
            Data = new Dictionary<string, object>();
        }

        public override string ToString()
        {
            var data = "";

            foreach(KeyValuePair<string, object> entry in Data)
            {
                data = data == "" ? $"({entry.Key} = {entry.Value})" : $"{data}, ({entry.Key} = {entry.Value})";
            }
                
            return $"{nameof(Type)}: {Type}, {nameof(Data)}: {data}";
        }
    }
}