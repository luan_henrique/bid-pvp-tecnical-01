using System;

namespace BidPVP.Scripts.PreStandard.LogicAuction
{
    [Serializable]
    public class StorageItem
    {
        public string name;
        public int Value;
    }
}