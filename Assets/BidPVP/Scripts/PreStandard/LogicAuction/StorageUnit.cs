using System;

namespace BidPVP.Scripts.PreStandard.LogicAuction
{
    [Serializable]
    public class StorageUnit
    {
        private const int NumStorageItems = 3;
        public const int BidIncrement = 200;
        public readonly StorageItem[] Items;
        public int CurrentBid;
        public Player Owner = null;

        public StorageUnit()
        {
            var rand = new System.Random(200000);
            Items = new StorageItem[NumStorageItems];
            for (var i = 0; i < NumStorageItems; i++)
            {
                Items[i] = new StorageItem();
            }
        }

        public void ChangeOwner(Player player)
        {
            Owner = player;
        }

        public void IncrementValue()
        {
            CurrentBid += BidIncrement;
        }

        public int GetTotalValue()
        {
            var totalValue = 0;
            foreach (var item in Items)
            {
                totalValue += item.Value;
            }

            return totalValue;
        }
    }
}