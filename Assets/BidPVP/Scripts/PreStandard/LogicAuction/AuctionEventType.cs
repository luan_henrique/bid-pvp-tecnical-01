using System;

namespace BidPVP.Scripts.PreStandard.LogicAuction
{
    [Serializable]
    public enum AuctionEventType
    {
        GameStart,
        Bid,
        StorageSold,
        GameEnd,
        PlayerJoin,
        PlayerLeave,
        NotifyRemainingTime,
        
        PlayerPressBid = 20
    }
}