using System;

namespace BidPVP.Scripts.PreStandard.LogicAuction
{
    
    [Serializable]
    public class Player
    {
        public string Name;
        public int Money;
        public int Earnings;
        public int SpentMoney;
        
        public bool Initiated()
        {
            return Name != "";
        }

        public void ClearPlayer()
        {
            Name = "";
            Money = 0;
            Earnings = 0;
            SpentMoney = 0;
        }

        public Player()
        {
            ClearPlayer();
        }
    }
}