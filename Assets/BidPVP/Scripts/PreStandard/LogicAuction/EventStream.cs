using System;
using System.Collections.Generic;

namespace BidPVP.Scripts.PreStandard.LogicAuction
{
    [Serializable]
    public class EventStream
    {
        private List<AuctionEvent> Stream;

        public EventStream()
        {
            Stream = new List<AuctionEvent>();
        }

        public AuctionEvent[] ConsumeStream()
        {
            var stream = Stream.ToArray();
            Stream.Clear();
            return stream;
        }
        
        public void AddEvent(AuctionEvent newAuctionEvent)
        {
            Stream.Add(newAuctionEvent);
        }
    }
}