using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace BidPVP.Scripts.PreStandard.LogicAuction
{
    [Serializable]
    public class Auction
    {
        private readonly int _numStorage = 3;
        private readonly int _numPlayers = 3;
        private readonly double _auctioneerSellTime;
        private const double AuctioneerOfferTime = 20000000000;

        private const double RemainingTimeNotificationStep = 1;
        private const double StartingRemainingTimeNotification = 3;

        private readonly Player[] _players;
        public readonly StorageUnit[] StorageUnits;
        private bool _started;
        public bool finished;
        public int currentStorage;
        private double _remainingTime;
        private double _nextNotificationTarget;
        
        public readonly EventStream Events;
        
        public Auction(double auctioneersTime = 5, int numStorage = 3, int numPlayers = 3)
        {
            _auctioneerSellTime = auctioneersTime;
            _numStorage = numStorage;
            _numPlayers = numPlayers;
            
            _players = new Player[numPlayers];
            for (var i = 0; i < numPlayers; i++)
            {
                _players[i] = new Player();
            }

            StorageUnits = new StorageUnit[_numStorage];
            for (var i = 0; i < _numStorage; i++)
            {
                StorageUnits[i] = new StorageUnit();
            }

            Events = new EventStream();
        }

        private int CountPlayers()
        {
            return _players.Count(player => player.Initiated());
        }

        private void AddPlayer(string name, int money)
        {
            for (var i = 0; i < _numPlayers; i++)
            {
                var player = _players[i];
                if (player.Initiated()) continue;
                player.Name = name;
                player.Money = money;
                Events.AddEvent(new AuctionEvent(AuctionEventType.PlayerJoin) {Data = {{"name", name}}});
                break;
            }
        }

        private void Start()
        {
            _started = true;
            _remainingTime = AuctioneerOfferTime;
            Events.AddEvent(new AuctionEvent(AuctionEventType.GameStart));
        }

        private void Finish()
        {
            finished = true;
            Events.AddEvent(new AuctionEvent(AuctionEventType.GameEnd));
        }

        public void RemovePlayer(string name)
        {
            GetPlayerOfName(name).ClearPlayer();
            Events.AddEvent(new AuctionEvent(AuctionEventType.PlayerLeave) {Data = {{"name", name}}});
        }

        public Player GetPlayerOfName(string name)
        {
            foreach (var player in _players)
            {
                if (player.Name == name) return player;
            }

            return null;
        }

        private void FinishStorage()
        {
            var currentStorage = StorageUnits[this.currentStorage];
            var storageWinner = currentStorage.Owner;
            storageWinner.Money -= currentStorage.CurrentBid;
            storageWinner.SpentMoney += currentStorage.CurrentBid;
            storageWinner.Earnings += currentStorage.GetTotalValue();
            this.currentStorage++;
            _remainingTime = AuctioneerOfferTime;
            _nextNotificationTarget = StartingRemainingTimeNotification;
            Events.AddEvent(new AuctionEvent(AuctionEventType.StorageSold)
            {
                Data =
                {
                    {"player", storageWinner.Name},
                    {"price", currentStorage.CurrentBid},
                    {"storageValue", currentStorage.GetTotalValue()}
                }
            });
            if (this.currentStorage == _numStorage)
            {
                Finish();
            }
        }

        public void PlayerJoin(string name, int money)
        {
            AddPlayer(name, money);
            if (CountPlayers() == _numPlayers)
            {
                Start();
            }
        }

        public void ReceiveBid(string playerName)
        {
            if ((!_started) || (finished))
                return;

            var bidder = GetPlayerOfName(playerName);
            var currentStorage = StorageUnits[this.currentStorage];

            if (bidder == currentStorage.Owner || (bidder.Money < (currentStorage.CurrentBid + StorageUnit.BidIncrement)))
                return;

            currentStorage.ChangeOwner(bidder);
            currentStorage.IncrementValue();
            Events.AddEvent(new AuctionEvent(AuctionEventType.Bid)
            {
                Data =
                {
                    {"player", playerName},
                    {"value", currentStorage.CurrentBid}
                }
            });
            _remainingTime = _auctioneerSellTime;
            _nextNotificationTarget = StartingRemainingTimeNotification;
        }

        public void PassTime(double delta)
        {
            _remainingTime -= delta;
            if (_remainingTime <= _nextNotificationTarget && _remainingTime >= 0)
            {
                Events.AddEvent(new AuctionEvent(AuctionEventType.NotifyRemainingTime){Data = {{"remainingRime", _nextNotificationTarget}}});
                _nextNotificationTarget -= RemainingTimeNotificationStep;
            }
            if (_remainingTime <= 0 && _started && !finished)
            {
                FinishStorage();
            }
        }
        
        public static Auction Deserialize(byte[] data)
        {
            var formatter = new BinaryFormatter();
            var memStream = new MemoryStream(data);
            
            var returnable =  (Auction)formatter.Deserialize(memStream);
            memStream.Close();
            return returnable;
        }

        public static byte[] Serialize(object customType)
        {
            var auction = (Auction)customType;
            var formatter = new BinaryFormatter();
            var memStream = new MemoryStream();
            formatter.Serialize(memStream, auction);
            
            var returnable = memStream.ToArray();
            memStream.Close();
            return returnable;
        }
    }
}