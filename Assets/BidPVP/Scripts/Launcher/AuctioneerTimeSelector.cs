﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace BidPVP.Scripts.Launcher
{
    [RequireComponent(typeof(InputField))]
    public class AuctioneerTimeSelector : MonoBehaviour
    {
        public static string AuctioneerTimePlayerPrefKey = "AuctioneerTimeDouble";

        private void Start()
        {

            float defaultTime = 5;
            var _inputField = GetComponent<InputField>();

            if (_inputField == null) return;
            if (PlayerPrefs.HasKey(AuctioneerTimePlayerPrefKey))
            {
                defaultTime = PlayerPrefs.GetFloat(AuctioneerTimePlayerPrefKey);
            }
            else
            {
                PlayerPrefs.SetFloat(AuctioneerTimePlayerPrefKey, defaultTime);
            }

            _inputField.text = defaultTime.ToString();
        }

        public void SetAuctioneerTime(string value)
        {
            if (float.TryParse(value, out var readResult))
            {
                PlayerPrefs.SetFloat(AuctioneerTimePlayerPrefKey, readResult);
            }
        }

    }
}
