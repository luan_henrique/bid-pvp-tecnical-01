﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace BidPVP.Scripts.Launcher
{
    [RequireComponent(typeof(InputField))]
    public class IntInputFieldSelector : MonoBehaviour
    {
        public string PlayerPrefOption = "NumberOfPlayers";
        public int defaultValue = 3;
        
        private void Start()
        {
            var _inputField = GetComponent<InputField>();

            if (_inputField == null) return;
            if (PlayerPrefs.HasKey(PlayerPrefOption)) 
            {
                defaultValue = PlayerPrefs.GetInt(PlayerPrefOption);
            }
            else
            {
                PlayerPrefs.GetInt(PlayerPrefOption, defaultValue);
            }

            _inputField.text = defaultValue.ToString();
        }

        public void SetValueOfPlayerPref(string value)
        {
            if (int.TryParse(value, out var readResult))
            {
                PlayerPrefs.SetInt(PlayerPrefOption, readResult);
            }
        }

    }
}
