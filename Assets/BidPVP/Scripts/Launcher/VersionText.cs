﻿using UnityEngine;
using UnityEngine.UI;

namespace BidPVP.Scripts.Launcher
{
    public class VersionText : MonoBehaviour
    {
        public TextAsset versionFile;
        public Text versionView;
        void Start()
        {
            var versionNumbers = versionFile.text.Split(' ');
            versionView.text = $"Version: {versionNumbers[0]}.{versionNumbers[1]}.{versionNumbers[2]}";
        }


    }
}