﻿using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BidPVP.Scripts.Launcher
{
    [RequireComponent(typeof(Dropdown))]
    public class GameVariantSelector
        : MonoBehaviour
    {
        public const string GameVersionPlayerPrefKey = "selectedVariant";
        public Dropdown gameVariantSelectorUi;

        private void Start()
        {
            gameVariantSelectorUi = this.GetComponent<Dropdown>();
    

            if (PlayerPrefs.HasKey(GameVersionPlayerPrefKey))
            {
                var variant = PlayerPrefs.GetString(GameVersionPlayerPrefKey);
                var dropDownIdx = -1;
                foreach (var optionData in gameVariantSelectorUi.options)
                {
                    if (optionData.text.Replace(" ", "") == variant)
                    {
                        dropDownIdx = gameVariantSelectorUi.options.IndexOf(optionData);
                        break;
                    }
                }

                gameVariantSelectorUi.value = dropDownIdx;
            }
            else
            {
                PlayerPrefs.SetString(GameVersionPlayerPrefKey, gameVariantSelectorUi.options[0].text.Replace(" ", ""));
            }
        }

        public void SetGameVariant(int versionIndex)
        {
            print($"set variant :D {versionIndex} - {gameVariantSelectorUi.options[versionIndex].text}");
            var selectedVersion = gameVariantSelectorUi.options[versionIndex].text;

            PlayerPrefs.SetString(GameVersionPlayerPrefKey, selectedVersion.Replace(" ", ""));
        }
    }
}