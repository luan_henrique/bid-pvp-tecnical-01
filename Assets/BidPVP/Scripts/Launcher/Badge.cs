﻿using UnityEngine;
using UnityEngine.UI;

namespace BidPVP.Scripts.Launcher
{
    public class Badge : MonoBehaviour
    {
        public Text Name;
        public GameObject Balloon;
        public Text BalloonText;
    
        public void SetBid(int value)
        {
            Balloon.SetActive(true);
            BalloonText.text = $"$ {value}";
        }

        public void CleanBalloon()
        {
            Balloon.SetActive(false);
        }
    
        public void SetName(string name)
        {
            Name.text = name;
        }
    }
}
