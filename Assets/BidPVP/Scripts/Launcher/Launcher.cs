﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

namespace BidPVP.Scripts.Launcher
{
#pragma warning disable 649


    public class Launcher : MonoBehaviourPunCallbacks
    {
        private bool usingMatchmaking = false;
        private static string roomNamePlayerPrefsKey = "MatchMakingRoom";

        [Tooltip("The Ui Panel to let the user enter name, connect and play")] [SerializeField]
        private GameObject controlPanel;

        [Tooltip("The Ui Text to inform the user about the connection progress")] [SerializeField]
        private Text feedbackText;

        [Tooltip("The maximum number of players per room")] [SerializeField]

        private bool _isConnecting;

        private const string GameVersion = "1";

        private void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        public void Connect()
        {
            _isConnecting = true;

            controlPanel.SetActive(false);

            if (PhotonNetwork.OfflineMode)
            {
                PhotonNetwork.CreateRoom(null, new RoomOptions {MaxPlayers = (byte)PlayerPrefs.GetInt("numPlayersHostConfig")});
            }
            else if (PhotonNetwork.IsConnected)
            {
                LogFeedback("Joining Room...");
                if (usingMatchmaking)
                {
                    PhotonNetwork.JoinRoom(PlayerPrefs.GetString(roomNamePlayerPrefsKey));
                }
                else
                {
                    PhotonNetwork.JoinRandomRoom();
                }
            }
            else
            {
                PhotonNetwork.GameVersion = GameVersion;
                PhotonNetwork.ConnectUsingSettings();
            }
        }

        private void LogFeedback(string message)
        {
            if (feedbackText == null)
            {
                return;
            }

            feedbackText.text += System.Environment.NewLine + message;
        }

        public override void OnConnectedToMaster()
        {
            controlPanel.SetActive(true);
            if (!_isConnecting) return;
            LogFeedback("<Color=Green>Connected</Color>");
            Connect();
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            var roomName = PlayerPrefs.GetString(roomNamePlayerPrefsKey);
            LogFeedback($"<Color=Red>OnJoinRFailed</Color>: Next -> Create {roomName} Room");
            PhotonNetwork.CreateRoom(roomName, new RoomOptions
                {MaxPlayers = (byte) PlayerPrefs.GetInt("numPlayersHostConfig", 3)});
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            LogFeedback("<Color=Red>OnJoinRandomFailed</Color>: Next -> Create a new Room");
            PhotonNetwork.CreateRoom(null, new RoomOptions
                {MaxPlayers = (byte) PlayerPrefs.GetInt("numPlayersHostConfig", 3)});
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            controlPanel.SetActive(true);
            LogFeedback("<Color=Red>OnDisconnected</Color> " + cause);
        }

        public override void OnJoinedRoom()
        {
            if (! PlayerPrefs.HasKey(GameVariantSelector.GameVersionPlayerPrefKey))
            {
                PlayerPrefs.SetString(GameVariantSelector.GameVersionPlayerPrefKey, "PreStandard");
            }
                
            LogFeedback($"<Color=Green>OnJoinedRoom</Color> with {PhotonNetwork.CurrentRoom.PlayerCount} Player(s)");
            PhotonNetwork.LocalPlayer.SetCustomProperties(new Hashtable()
            {
                {"startingMoney", PlayerPrefs.GetInt("startingMoney")}
            });
            PhotonNetwork.LoadLevel($"Storages_{PlayerPrefs.GetString(GameVariantSelector.GameVersionPlayerPrefKey)}");
        }

        public void setRoomName(string roomName)
        {
            if (string.IsNullOrEmpty(roomName))
            {
                usingMatchmaking = false;
            }
            else
            {
                PlayerPrefs.SetString(roomNamePlayerPrefsKey, roomName);
                usingMatchmaking = true;
            }
        }
    }
}