﻿using System.Collections.Generic;
using UnityEngine;

namespace BidPVP.Scripts.DataModules
{
    [CreateAssetMenu(fileName = "StorageBackItems", menuName = "StorageBackItems")]
    public class storageBackItems : ScriptableObject
    {
        [SerializeField] public List<storageItem> StorageBackItems;
    }
}