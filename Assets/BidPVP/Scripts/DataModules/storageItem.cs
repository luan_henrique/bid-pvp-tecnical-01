﻿using UnityEngine;

namespace BidPVP.Scripts.DataModules
{
    [CreateAssetMenu(fileName = "StorageItem", menuName = "StorageItem")]
    public class storageItem : ScriptableObject
    {
        [SerializeField] public GameObject itemGameObject;

        [SerializeField] public int itemPrice;
    }
}