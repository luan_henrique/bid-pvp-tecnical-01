﻿using System.Collections.Generic;
using UnityEngine;

namespace BidPVP.Scripts.DataModules
{
    [CreateAssetMenu(fileName = "StorageMiddleItems", menuName = "StorageMiddleItems")]
    public class storageMiddleItems : ScriptableObject
    {
        [SerializeField] public List<storageItem> StorageMiddleItems;
    }
}