﻿using System.Collections.Generic;
using UnityEngine;

namespace BidPVP.Scripts.DataModules
{
    [CreateAssetMenu(fileName = "StorageFrontItems", menuName = "StorageFrontItems")]
    public class storageFrontItems : ScriptableObject
    {
        [SerializeField] public List<storageItem> StorageFrontItems;
    }
}